/*
Copyright (C) 2017 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use reqwest;

#[derive(Fail, Debug)]
pub enum Error {
	#[fail(display = "Invalid operation called")]
	NotCallable,

	#[fail(display = "Reqwest error: {}", _0)]
	Reqwest(reqwest::Error),

	#[fail(display = "Empty response received")]
	EmptyResponse,

	#[fail(display = "Invalid API key")]
	InvalidApiKey,

	#[fail(display = "Not authorized")]
	NotAuthorized,

	#[fail(display = "JSON decoding error: {}", _0)]
	Json(String),

	#[fail(display = "Unknown error: {}", _0)]
	Unknown(String),
}

impl Error {
	pub fn new(msg: &str) -> Error {
		match msg {
			"Invalid API key." => Error::InvalidApiKey,
			"not authorized" => Error::NotAuthorized,
			_ => Error::Unknown(msg.to_string()),
		}
	}
}

impl<'a> From<&'a str> for Error {
	fn from(msg: &'a str) -> Self {
		Error::new(msg)
	}
}
