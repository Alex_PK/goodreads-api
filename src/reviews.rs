/*
Copyright (C) 2017 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

use serde_json;

use errors::Error;
use super::{Goodreads, Request, BaseRequest};
use values::{Sort, Order};


//#[derive(Debug, Default, Serialize, Deserialize)]
//pub struct Shelf {
//	pub id: u32,
//	pub name: String,
//	pub book_count: u32,
//	pub exclusive_flag: bool,
//	pub description: Option<String>,
//	pub sort: Option<Sort>,
//	pub order: Option<Order>,
//	pub per_page: Option<u32>,
//	// TODO: find out what display_fields really is
//	pub display_fields: String,
//	pub featured: bool,
//	pub recommend_for: bool,
//	pub sticky: Option<bool>,
//}

///*
// *  Private structs to mimic the response from Goodreads
// */
//#[derive(Debug, Default, Serialize, Deserialize)]
//struct ShelvesResponse {
//	pub shelves: ShelvesResponseShelves,
//}
//
//#[derive(Debug, Default, Serialize, Deserialize)]
//struct ShelvesResponseShelves {
//	pub start: i32,
//	pub end: i32,
//	pub user_shelves: Vec<Shelf>,
//}

pub struct ReviewsRequest<'r>(BaseRequest<'r>);

impl<'r> Request for ReviewsRequest<'r> {
	type Response = String;

	fn get(&self) -> Result<Self::Response, Error> {
		self.0.get()
	}
}


impl Goodreads {
	pub fn reviews<'r>(&'r self, user_id: u32) -> ReviewsRequest<'r> {
		ReviewsRequest(
			BaseRequest::new(self, "/review/list")
				.with("v", 2)
				.with("user_id", user_id)
		)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	const API_KEY: &'static str = include_str!("../api_key.txt");

	#[test]
	fn test_reviews_request() {
		let gr = Goodreads::new(API_KEY);
		let res = gr.reviews(616321).get();
		println!("\n{:?}", res);
	}
}
