use serde::de::{Deserialize, Deserializer, Visitor, Error};
use serde::ser::{Serialize, Serializer};

#[derive(Debug)]
pub enum Sort {
	Title,
	Author,
	Cover,
	Rating,
	YearPub,
	DatePub,
	DatePubEdition,
	DateStarted,
	DateRead,
	DateUpdated,
	DateAdded,
	Recommender,
	AvgRating,
	NumRatings,
	Review,
	ReadCount,
	Votes,
	Random,
	Comments,
	Notes,
	Isbn,
	Isbn13,
	Asin,
	NumPages,
	Format,
	Position,
	Shelves,
	Owned,
	DatePurchased,
	PurchaseLocation,
	Condition,
}

impl ToString for Sort {
	fn to_string(&self) -> String {
		match *self {
			Sort::Title => "title",
			Sort::Author => "author",
			Sort::Cover => "cover",
			Sort::Rating => "rating",
			Sort::YearPub => "year_pub",
			Sort::DatePub => "date_pub",
			Sort::DatePubEdition => "date_pub_edition",
			Sort::DateStarted => "date_started",
			Sort::DateRead => "date_read",
			Sort::DateUpdated => "date_updated",
			Sort::DateAdded => "date_added",
			Sort::Recommender => "recommender",
			Sort::AvgRating => "avg_rating",
			Sort::NumRatings => "num_ratings",
			Sort::Review => "review",
			Sort::ReadCount => "read_count",
			Sort::Votes => "votes",
			Sort::Random => "random",
			Sort::Comments => "comments",
			Sort::Notes => "notes",
			Sort::Isbn => "isbn",
			Sort::Isbn13 => "isbn13",
			Sort::Asin => "asin",
			Sort::NumPages => "num_pages",
			Sort::Format => "format",
			Sort::Position => "position",
			Sort::Shelves => "shelves",
			Sort::Owned => "owned",
			Sort::DatePurchased => "date_purchased",
			Sort::PurchaseLocation => "purchase_location",
			Sort::Condition => "condition",
		}.to_string()
	}
}

impl<'a> From<&'a str> for Sort {
	fn from(v: &'a str) -> Self {
		match v.to_lowercase().as_str() {
			"title" => Sort::Title,
			"author" => Sort::Author,
			"cover" => Sort::Cover,
			"rating" => Sort::Rating,
			"year_pub" => Sort::YearPub,
			"date_pub" => Sort::DatePub,
			"date_pub_edition" => Sort::DatePubEdition,
			"date_started" => Sort::DateStarted,
			"date_read" => Sort::DateRead,
			"date_updated" => Sort::DateUpdated,
			"date_added" => Sort::DateAdded,
			"recommender" => Sort::Recommender,
			"avg_rating" => Sort::AvgRating,
			"num_ratings" => Sort::NumRatings,
			"review" => Sort::Review,
			"read_count" => Sort::ReadCount,
			"votes" => Sort::Votes,
			"random" => Sort::Random,
			"comments" => Sort::Comments,
			"notes" => Sort::Notes,
			"isbn" => Sort::Isbn,
			"isbn13" => Sort::Isbn13,
			"asin" => Sort::Asin,
			"num_pages" => Sort::NumPages,
			"format" => Sort::Format,
			"position" => Sort::Position,
			"shelves" => Sort::Shelves,
			"owned" => Sort::Owned,
			"date_purchased" => Sort::DatePurchased,
			"purchase_location" => Sort::PurchaseLocation,
			"condition" => Sort::Condition,
			_ => Sort::Title,
		}
	}
}

impl Serialize for Sort {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
		where S: Serializer
	{
		serializer.serialize_str(&self.to_string())
	}
}

impl<'de> Deserialize<'de> for Sort {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
		where D: Deserializer<'de>
	{
		use std::fmt;

		struct FieldVisitor;

		impl<'de> Visitor<'de> for FieldVisitor {
			type Value = Sort;

			fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
				formatter.write_str("a string representing the sorting method")
			}

			fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
				where E: Error
			{
				Ok(value.into())
			}
		}

		deserializer.deserialize_str(FieldVisitor)
	}
}

impl From<String> for Sort {
	fn from(v: String) -> Self {
		v.as_str().into()
	}
}


#[derive(Debug)]
pub enum Order {
	Asc,
	Desc,
}

impl ToString for Order {
	fn to_string(&self) -> String {
		match *self {
			Order::Asc => "a",
			Order::Desc => "d",
		}.to_string()
	}
}

impl<'a> From<&'a str> for Order {
	fn from(v: &'a str) -> Self {
		match v.to_lowercase().as_str() {
			"a" => Order::Asc,
			"d" => Order::Desc,
			_ => Order::Asc,
		}
	}
}

impl From<String> for Order {
	fn from(v: String) -> Self {
		v.as_str().into()
	}
}

impl Serialize for Order {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
		where S: Serializer
	{
		serializer.serialize_str(&self.to_string())
	}
}

impl<'de> Deserialize<'de> for Order {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
		where D: Deserializer<'de>
	{
		use std::fmt;

		struct FieldVisitor;

		impl<'de> Visitor<'de> for FieldVisitor {
			type Value = Order;

			fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
				formatter.write_str("a string representing the sorting method")
			}

			fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
				where E: Error
			{
				Ok(value.into())
			}
		}

		deserializer.deserialize_str(FieldVisitor)
	}
}
