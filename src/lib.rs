/*
Copyright (C) 2017 Alessandro Pellizzari

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#[macro_use]
extern crate failure;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

extern crate minidom;
extern crate reqwest;
extern crate percent_encoding;

use std::sync::Mutex;
use std::time::{SystemTime, Duration, UNIX_EPOCH};
use reqwest::{Client, StatusCode};

mod errors;
pub mod params;
pub mod values;

pub mod shelves;
pub mod reviews;

use errors::Error;
use params::Params;

const URL: &'static str = "https://www.goodreads.com";

pub struct Goodreads {
	api_key: String,
	client: Client,
	last_call: Mutex<u64>,
}

impl Goodreads {
	pub fn new(api_key: &str) -> Goodreads {
		Goodreads {
			api_key: api_key.to_string(),
			client: Client::new(),
			last_call: Mutex::new(0),
		}
	}

	// Goodreads allow only 1 call per second.
	// TODO: make it configurable
	fn wait_if_needed(&self) {
		self.last_call.lock()
			.map(|mut last_call| {
				let diff = Goodreads::now_ms() - *last_call;
				if diff < 1000 {
					std::thread::sleep(Duration::from_millis(diff));
				}
				*last_call = Goodreads::now_ms();
			});
	}

	fn now_ms() -> u64 {
		let start = SystemTime::now();
		let since_the_epoch = start.duration_since(UNIX_EPOCH).expect("Time went backwards");

		since_the_epoch.as_secs() * 1000 +
			since_the_epoch.subsec_nanos() as u64 / 1_000_000
	}
}

trait Request {
	type Response;
	fn get(&self) -> Result<Self::Response, Error> {
		Err(Error::NotCallable)
	}

	fn post(&self) -> Result<Self::Response, Error> {
		Err(Error::NotCallable)
	}
}

struct BaseRequest<'r> {
	gr: &'r Goodreads,
	url: &'r str,
	params: Params,
}

impl<'r> BaseRequest<'r> {
	pub fn new(gr: &'r Goodreads, url: &'r str) -> BaseRequest<'r> {
		BaseRequest {
			gr,
			url,
			params: Params::new(),
		}
	}

	pub fn with<T>(mut self, k: &str, v: T) -> Self
		where T: ToString
	{
		self.params.set(k, v);
		self
	}

	pub fn set<T>(&mut self, k: &str, v: T) -> &Self
		where T: ToString
	{
		self.params.set(k, v);
		self
	}
}

impl<'r> Request for BaseRequest<'r> {
	type Response = String;
	fn get(&self) -> Result<Self::Response, Error> {
		self.gr.wait_if_needed();
		self.gr.client.get(&format!("{}{}?key={}&{}", URL, self.url, self.gr.api_key, self.params.to_querystring()))
			.send()
			.map_err(|e| Error::Reqwest(e))
			.and_then(|mut res| {
				let body = res.text().map_err(|e| Error::Reqwest(e))?;

				match res.status() {
					StatusCode::Ok => res.text().map_err(|e| Error::Reqwest(e)),
					StatusCode::Unauthorized => {
						let cr = body.find('\n').unwrap_or_else(|| body.len());
						Err(match &body[0..cr] {
							"Invalid API key." => Error::InvalidApiKey,
							"not authorized" => Error::NotAuthorized,
							_ => Error::Unknown(body.to_string()),
						})
					},
					_ => Err(Error::from("Unexpected error")),
				}
			})
			.and_then(|res| {
				if res.is_empty() {
					Err(Error::EmptyResponse)
				} else if res.chars().nth(0).unwrap().is_alphanumeric() {
					let cr = res.find('\n').unwrap_or_else(|| res.len());
					Err(Error::new(&res[0..cr]))
				} else {
					Ok(res)
				}
			})
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	const API_KEY: &'static str = include_str!("../api_key.txt");

	#[test]
	fn test_instantiate() {
		let _ = Goodreads::new(API_KEY);
	}
}
